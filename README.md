# Inleiding
Dit project is een voorbeeld implementatie van de Use Case _Tickets bestellen_ uit de _Culturele Evenementen_ case voor het vak _Software Engineering 1 / ontwerp_ 
# Download opties
- je kan downloaden als zip
- je kan downloaden met git
    1. installeer git
    2. op de commandolijn:
    
        ```git clone https://gitlab.com/kdg-ti/software-engineering-1/culturele-evenementen.git```
- je kan downloaden via intellij
    - new > Project from version control... 
        - URL: https://gitlab.com/kdg-ti/software-engineering-1/culturele-evenementen.git
# Uitvoeren
Open het project via IntelliJ IDEA, eventueel moet je Java JDK  11 instellen
De main bevindt zich in be.kdg.se1.evenementen.CultureleEvenementen