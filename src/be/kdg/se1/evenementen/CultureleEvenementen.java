package be.kdg.se1.evenementen;

import be.kdg.se1.evenementen.controller.TicketReservatieController;
import be.kdg.se1.evenementen.domein.klant.KlantenRepository;
import be.kdg.se1.evenementen.domein.voostelling.EvenementenRepository;
import be.kdg.se1.evenementen.domein.verkoop.VerkoopRepository;
import be.kdg.se1.evenementen.data.DataLader;
import be.kdg.se1.evenementen.ui.bestelTicketsUI;

public class CultureleEvenementen {
	KlantenRepository klanten = new KlantenRepository();
	EvenementenRepository evenementen = new EvenementenRepository();
	VerkoopRepository verkopen = new VerkoopRepository();

	public static void main(String[] args) {
		CultureleEvenementen app = new CultureleEvenementen();
		app.laadData();
		app.startUI();
	}

	private void laadData() {
		DataLader lader = new DataLader(klanten,evenementen);
		lader.laadKlanten();
		lader.laadVoorstellingen();
		lader.verkoopTickets(0.9);
	}

	private void startUI() {
		bestelTicketsUI ui = new bestelTicketsUI(new TicketReservatieController(klanten,evenementen,verkopen));
		ui.bestel();
	}



}
