package be.kdg.se1.evenementen.controller;

import be.kdg.se1.evenementen.domein.klant.Klant;
import be.kdg.se1.evenementen.domein.klant.KlantenRepository;
import be.kdg.se1.evenementen.domein.verkoop.*;
import be.kdg.se1.evenementen.domein.voostelling.EvenementenRepository;
import be.kdg.se1.evenementen.domein.verkoop.VerkoopRepository;
import be.kdg.se1.evenementen.domein.voostelling.Voorstelling;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static be.kdg.se1.evenementen.domein.verkoop.ProductType.TICKET;
import static be.kdg.se1.evenementen.domein.verkoop.TicketReservatie.Toestand.NIEUW;

public class TicketReservatieController {
	private final KlantenRepository klanten;
	private final EvenementenRepository evenementen;
	private final VerkoopRepository verkopen;
	private Klant huidigeKlant;
	private TicketVerkoop huidigeVerkoop;
	private Voorstelling huidigeVoorstelling;

	public TicketReservatieController(
		KlantenRepository klanten,
		EvenementenRepository evenementen,
		VerkoopRepository verkopen) {
		this.klanten = klanten;
		this.evenementen=evenementen;
		this.verkopen=verkopen;
	}

	public boolean aanmelden(String email, String paswoord) {
		huidigeKlant = klanten.getKlant(email);
		return huidigeKlant.meldAan(paswoord);
	}

	public void maakTicketVerkoop() {
		huidigeVerkoop = new TicketVerkoop(huidigeKlant,Verkoop.Wijze.ONLINE );
	}

	public Klant getKlant() {
		return huidigeKlant;
	}

	public TicketVerkoop getVerkoop() {
		return huidigeVerkoop;
	}

	public List<Voorstelling> kiesVoorstellingen(LocalDate datum) {
		return evenementen.getAllVoorstellingen(datum);
	}

	public List<TicketReservatie> selecteerVoorstelling(Voorstelling gekozenVoorstelling) {
		huidigeVoorstelling = gekozenVoorstelling;
		return gekozenVoorstelling.getTickets(TICKET,NIEUW);
	}

	public List<TicketReservatie> selecteerTickets(
		Map<KortingType, Integer> bestelling,
		int rang,
		ProductType type) {
				int totaal = 0;
				for (int aantal:bestelling.values()) {
					totaal+=aantal;
				}
		    List<TicketReservatie>  tickets =  huidigeVoorstelling.getTickets(type, NIEUW,  rang,totaal);
				huidigeVerkoop.addProducts(bestelling,tickets);
				return tickets;
	}

	public int eindeBestelling() {
		huidigeVerkoop.setCompleet(true);
		return huidigeVerkoop.getTotal();
	}

	public Betaling betaal(int bedrag, Betaling.Wijze betaalwijze) {
		Betaling resultaat = huidigeVerkoop.betaal(bedrag,betaalwijze);
		verkopen.insert(huidigeVerkoop);
		return resultaat;
	}
}
