package be.kdg.se1.evenementen.data;

import be.kdg.se1.evenementen.domein.infrastructuur.Adres;
import be.kdg.se1.evenementen.domein.infrastructuur.Locatie;
import be.kdg.se1.evenementen.domein.infrastructuur.Zaal;
import be.kdg.se1.evenementen.domein.klant.Klant;
import be.kdg.se1.evenementen.domein.klant.KlantenRepository;
import be.kdg.se1.evenementen.domein.verkoop.TicketReservatie;
import be.kdg.se1.evenementen.domein.verkoop.TicketVerkoop;
import be.kdg.se1.evenementen.domein.verkoop.Verkoop;
import be.kdg.se1.evenementen.domein.voostelling.Evenement;
import be.kdg.se1.evenementen.domein.voostelling.EvenementenRepository;
import be.kdg.se1.evenementen.domein.voostelling.Voorstelling;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static be.kdg.se1.evenementen.domein.verkoop.KortingType.INTERN;
import static be.kdg.se1.evenementen.domein.verkoop.ProductType.TICKET;

/**
 * Author: derijkej
 */
public class DataLader {

	KlantenRepository klanten;
	EvenementenRepository evenementen;
	public DataLader(KlantenRepository klanten, EvenementenRepository evenementen) {
		this.klanten=klanten;
		this.evenementen=evenementen;
	}

	public void laadVoorstellingen() {
		Locatie deSingel = new Locatie(
			"De Singel",
			new Adres("Desguinlei", "25", 2018, "Antwerpen"),
			"03 248 28 28",
			"info@desingel.be");
		Zaal rodeZaal = new Zaal("Rode Zaal", 800,deSingel);
		Zaal blauweZaal = new Zaal("Blauwe Zaal", 900, deSingel);
		Evenement lotti = new Evenement("Lotti 50",
			"Concertreeks ter gelegenheid van de 50e verjaardag van Helmut Lotti",
			LocalDate.of(2020, 9, 21),
			LocalDate.of(2020, 10, 10), 2021);
		lotti.addVoorstelling(new Voorstelling(
			"Soul Classics in Symphony",
			"Een spannende muzikale terugblik op het soul-tijdperk.",
			LocalDateTime.of(2020, 9, 21, 20, 30),
			150,rodeZaal));
		lotti.addVoorstelling(new Voorstelling(
			"Helmut Lotti goes classic",
			"Lotti zal optreden met het versterkt Golden Symphonic Orchestra onder leiding van André Walschaerts (directeur van de muziekacademie van Heist-op-den-Berg).",
			LocalDateTime.of(2020, 9, 25, 20, 30),
			150,blauweZaal));
		evenementen.add(lotti);
		Evenement figaro = new Evenement("Figaro cyclus",
			"Operas op basis van de stukken van Pierre Beaumarchais",
			LocalDate.of(2020, 9, 25),
			LocalDate.of(2020, 9, 30), 2021);
		figaro.addVoorstelling(new Voorstelling(
			"De barbier van Sevilla",
			"De beroemde opera van Rossini.",
			LocalDateTime.of(2020, 9, 25, 20, 30),
			180,rodeZaal));
		figaro.addVoorstelling(new Voorstelling(
			"De bruiloft van Figaro",
			"De beroemde opera van Mozart.",
			LocalDateTime.of(2020, 9, 30, 20, 30),
			180,blauweZaal));
		evenementen.add(figaro);
	}

	public void laadKlanten() {
		klanten.add("henk.wijngaard@student.kdg.be", new Klant(
			1,
			"Henk Wijngaard",
			new Adres("Plezantstraat", "12B", 2160, "Borsbeek"),
			LocalDate.of(1960, 4, 1),
			"henk.wijngaard@student.kdg.be",
			"geheim")
		);
		klanten.add("tina.trucker@student.kdg.be", new Klant(
			2,
			"Tina Trucker",
			new Adres("Paddeschootdreef", "122", 9150, "Verrebroek"),
			LocalDate.of(1970, 12, 27),
			"tina.trucker@student.kdg.be",
			"secret")
		);
	}

	/**
	 * Markeert een aantal tickets als verkocht
	 * @param bezetting ratio van verkochte tickets
	 */
	public void verkoopTickets(double bezetting) {
		// bezetting afronden naar tienden
		int rate = (int) (bezetting*10);
		// Dummy verkoop om een aantal plaatsen te bezetten
		TicketVerkoop verkoop = new TicketVerkoop(null, Verkoop.Wijze.BUREAU);
		for (Evenement ev:evenementen.getAll()){
			for (Voorstelling v:ev.getVoorstellingen()){
				// zou in principe alleen voor niet abonnementstickets mogen gebeuren
				for (TicketReservatie ticket : v.getTickets(TICKET)){
					if (ticket.getTicketnr() % 10 < rate) verkoop.addProduct(INTERN,ticket);
				}
			}
		}
	}
}
