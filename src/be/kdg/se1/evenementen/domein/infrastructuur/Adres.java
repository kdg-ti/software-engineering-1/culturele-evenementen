package be.kdg.se1.evenementen.domein.infrastructuur;

public class Adres {

	private String straat;
	private String huisnr;
	private int 	postcode;
	private String	gemeente;

	public Adres(String straat, String huisnr, int postcode, String gemeente) {
		this.straat = straat;
		this.huisnr = huisnr;
		this.postcode = postcode;
		this.gemeente = gemeente;
	}
}
