package be.kdg.se1.evenementen.domein.infrastructuur;

public class Locatie {
	private String naam;
	private 	Adres adres;
	private String	tel;
	private String email;

	public Locatie(String naam, Adres adres, String tel, String email) {
		this.naam = naam;
		this.adres = adres;
		this.tel = tel;
		this.email = email;
	}
}
