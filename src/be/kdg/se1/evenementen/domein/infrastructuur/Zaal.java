package be.kdg.se1.evenementen.domein.infrastructuur;

import java.util.*;

import static be.kdg.se1.evenementen.domein.verkoop.KortingType.*;
import static be.kdg.se1.evenementen.domein.verkoop.ProductType.*;

import java.util.ArrayList;
import java.util.List;

public class Zaal {
	private String zaalnaam;
	private int capaciteit;
	private Locatie locatie;
	// zou efficienter kunnen zijn
	// private Map<ZetelRang,List<Zetel>> zetels ;
	private List<Zetel> zetels = new ArrayList<>();

	private List<ZetelRang> rangen = List.of(
		new ZetelRang(1, 13, 18, TICKET, Map.of(BASIS,37,MIN19,10,MIN25,32,PLUS65,32)),
		new ZetelRang(2, 19, 21, TICKET,Map.of(BASIS,32,MIN19,10,MIN25,27,PLUS65,27)),
		new ZetelRang(3, 1, 4, TICKET,Map.of(BASIS,27,MIN19,10,MIN25,22,PLUS65,22)),
		new ZetelRang(1, 8, 12, ABONNEMENT,Map.of(BASIS,132,MIN19,36,MIN25,116,PLUS65,116)),
		new ZetelRang(2, 22, 23, ABONNEMENT,Map.of(BASIS,116,MIN19,36,MIN25,100,PLUS65,100)),
		new ZetelRang(3, 5, 7, ABONNEMENT,Map.of(BASIS,100,MIN19,36,MIN25,100,PLUS65,80))
	);
	private  final ZetelRang LAATSTE_RANG = rangen.get(4);

	public Zaal(String zaalnaam, int capaciteit, Locatie locatie) {
		this.zaalnaam = zaalnaam;
		this.capaciteit = capaciteit;
		this.locatie = locatie;
		initZetels();
	}

	private void initZetels() {
		int zetelsPerRij = capaciteit / LAATSTE_RANG.getEindRij();
		int zetelNummer = 1;
		for (ZetelRang rang : rangen) {
			for (int rij = rang.getBeginRij(); rij <= rang.getEindRij(); rij++) {
				for (int kolom = 1; kolom <= zetelsPerRij; kolom++) {
					zetels.add(new Zetel(zetelNummer++, rij, rang));
				}
			}
		}
		// overige zetels op laatste rij
		for (;zetelNummer<=capaciteit;zetelNummer++){
			zetels.add(new Zetel(zetelNummer,LAATSTE_RANG.getEindRij(),LAATSTE_RANG));
		}
	}


	public List <Zetel> getZetels() {
		return zetels;
	}

	public List<ZetelRang> getRangen() {
		return rangen;
	}

	@Override
	public String toString() {
		return "Zaal{" +
			"zaalnaam='" + zaalnaam + '\'' +
			", capaciteit=" + capaciteit +
			'}';
	}
}
