package be.kdg.se1.evenementen.domein.infrastructuur;

public class Zetel {
	private int zetelnr;
	private int rijnr;
	private ZetelRang rang;

	public Zetel(int zetelnr, int rijnr,ZetelRang rang) {
		this.zetelnr = zetelnr;
		this.rijnr = rijnr;
		this.rang=rang;
	}

	public ZetelRang getRang() {
		return rang;
	}

	public int getZetelnr() {
		return zetelnr;
	}

	public int getRijnr() {
		return rijnr;
	}

	@Override
	public String toString() {
		return "Zetel{" +
			"zetelnr=" + zetelnr +
			", rijnr=" + rijnr +
			", rang=" + rang +
			'}';
	}
}
