package be.kdg.se1.evenementen.domein.infrastructuur;

import be.kdg.se1.evenementen.domein.verkoop.KortingType;
import be.kdg.se1.evenementen.domein.verkoop.ProductType;

import java.util.Map;

import static be.kdg.se1.evenementen.domein.verkoop.KortingType.INTERN;

public class ZetelRang {
	private final ProductType type;
	private int rang;
	private String	beschrijving;
	private int beginRij;
	private int eindRij;
	private Map<KortingType,Integer> tarieven;


	public ZetelRang(
		int rang,
		int beginrij,
		int eindRij,
		ProductType type,
		Map<KortingType,Integer> tarieven) {
		this.rang = rang;
		this.beschrijving = "Rang " + rang + " " + type;
		this.beginRij = beginrij;
		this.eindRij = eindRij;
		this.tarieven=tarieven;
		this.type=type;

	}

	public int getRangNummer() {
		return rang;
	}

	public String getBeschrijving() {
		return beschrijving;
	}

	public int getBeginRij() {
		return beginRij;
	}

	public int getEindRij() {
		return eindRij;
	}

	public ProductType getType() {
		return type;
	}


	public int getPrijs(KortingType korting) {
		return  (korting == INTERN) ? 0:	tarieven.get(korting);
	}

	@Override
	public String toString() {
		return "ZetelRang{" +
			"type=" + type +
			", rang=" + rang +
			'}';
	}
}
