package be.kdg.se1.evenementen.domein.klant;

import be.kdg.se1.evenementen.domein.infrastructuur.Adres;

import java.time.LocalDate;

public class Klant {
	private int klantnr;
	private String naam;
	private Adres adres;
		private LocalDate gebdatum;
	private String	email;
		private String paswoord;

	public Klant(int klantnr, String naam, Adres adres, LocalDate gebdatum, String email, String paswoord) {
		this.klantnr = klantnr;
		this.naam = naam;
		this.adres = adres;
		this.gebdatum = gebdatum;
		this.email = email;
		this.paswoord = paswoord;
	}

	public boolean meldAan(String paswoord) {
		return this.paswoord != null && this.paswoord.equals(paswoord);
	}

	@Override
	public String toString() {
		return "Klant{" +
			"naam='" + naam + '\'' +
			", email='" + email + '\'' +
			'}';
	}
}
