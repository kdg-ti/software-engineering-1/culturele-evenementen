package be.kdg.se1.evenementen.domein.klant;

import java.util.HashMap;
import java.util.Map;

public class KlantenRepository {
	private Map<String, Klant> entities = new HashMap<>();

	public void add(String mail, Klant klant){
		entities.put(mail, klant);
	}

	public Klant getKlant(String email) {
		return entities.get(email);
	}
}
