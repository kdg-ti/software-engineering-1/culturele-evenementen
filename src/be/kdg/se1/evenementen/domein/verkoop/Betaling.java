package be.kdg.se1.evenementen.domein.verkoop;

import java.util.UUID;

/**
 * Author: derijkej
 */
public class Betaling {
	UUID betaalnr;
	int bedrag;
	Wijze betaalwijze;

	public Betaling(int bedrag, Wijze wijze) {
		betaalnr = UUID.randomUUID();
		this.bedrag = bedrag;
		this.betaalwijze = wijze;
	}

	public enum Wijze{
		OVERSCHRIJVING,
		CONTANT,
		BANKKAART,
		KREDIETKAART
	}

	@Override
	public String toString() {
		return "Betaling{" +
			"betaalnr=" + betaalnr +
			", bedrag=" + bedrag +
			", betaalwijze=" + betaalwijze +
			'}';
	}
}
