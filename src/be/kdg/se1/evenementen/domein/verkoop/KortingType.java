package be.kdg.se1.evenementen.domein.verkoop;

public enum KortingType {
	INTERN,
	BASIS,
	MIN19,
	MIN25,
	PLUS65
}
