package be.kdg.se1.evenementen.domein.verkoop;

import be.kdg.se1.evenementen.domein.infrastructuur.Zetel;
import be.kdg.se1.evenementen.domein.infrastructuur.ZetelRang;
import be.kdg.se1.evenementen.domein.voostelling.Voorstelling;

import static be.kdg.se1.evenementen.domein.verkoop.TicketReservatie.Toestand.*;

import java.time.LocalDateTime;

public class TicketReservatie {
	private int ticketnr;
	private int prijs;
	private Zetel zetel;
	//private TicketVerkoop verkoop=null;
	private Voorstelling voorstelling;
	// kan ook als afgeleid attribuut geïmplementeerd worden
	private Toestand toestand;

	public TicketReservatie(Zetel zetel, Voorstelling voorstelling) {
		this(zetel.getZetelnr(), zetel, voorstelling);
	}

	public void reserveer(KortingType korting) {
		this.prijs = getZetel().getRang().getPrijs(korting);
		toestand = GERESERVEERD;
	}

	public ProductType getType() {
		return getZetel().getRang().getType();
	}

	public ZetelRang getRang() {
		return zetel.getRang();
	}

	public boolean matches(Toestand toestand, ProductType type) {
		return matches(toestand, type,-1);
	}

	public enum Toestand {
		NIEUW,
		GERESERVEERD,
		BETAALD,
		GELEVERD,
		BIJGEWOOND,
		NIET_BIJGEWOOND,
		ENQUETE_INGEVULD
	}

	/**
	 * Deze methode houdt geen rekening met abonnementen
	 *
	 * @return Toestand waarin het ticket verkeert
	 */
	public Toestand getToestand() {
		return toestand;
	}

	public TicketReservatie(int i, Zetel zetel, Voorstelling voorstelling) {
		ticketnr = i;
		this.zetel = zetel;
		this.voorstelling = voorstelling;
		toestand = NIEUW;
	}

	public int getTicketnr() {
		return ticketnr;
	}

	public int getPrijs() {
		return prijs;
	}

	public Zetel getZetel() {
		return zetel;
	}

	public boolean matches(Toestand toestand,ProductType type, int rang) {
		return (type == null || type == getType())
			&& (rang < 1 || rang == getRang().getRangNummer())
			&& (toestand == null || getToestand() == toestand);
	}

	@Override
	public String toString() {
		return "TicketReservatie{" +
			", ticketnr=" + ticketnr +
			", prijs=" + prijs +
			", zetel=" + zetel +
			", voorstelling=" + voorstelling +
			", toestand=" + toestand +
			'}';
	}
}
