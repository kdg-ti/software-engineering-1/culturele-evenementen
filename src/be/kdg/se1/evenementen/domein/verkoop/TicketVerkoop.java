package be.kdg.se1.evenementen.domein.verkoop;

import be.kdg.se1.evenementen.domein.klant.Klant;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TicketVerkoop extends Verkoop {
	private List<TicketReservatie> items = new ArrayList<>();
	public TicketVerkoop(Klant klant, Wijze wijze) {
		super(klant, wijze);
	}

	@Override
	public void add(Object product) {
		items.add((TicketReservatie)product);
	}

	/**
	 *
	 * @param bestelling aantal bestelling per kortingtype
	 * @param tickets de te verkopen tickets. Aantal moet exact overeenkomen met totaal aantal bestellingen
	 */
	public void addProducts(
		Map<KortingType, Integer> bestelling,
		List<TicketReservatie> tickets) {
		int ticketNr=0;
		for (Map.Entry<KortingType, Integer> bestellijn : bestelling.entrySet()){
			for (int i = 0; i< bestellijn.getValue();i++){
				addProduct(bestellijn.getKey(),tickets.get(ticketNr++));
			}
		}

	}

	public void addProduct(KortingType type, TicketReservatie ticket) {
		ticket.reserveer(type);
		items.add(ticket);
	}

	public int getTotal() {
		int total = 0;
		for (TicketReservatie ticket : items){
			total+=ticket.getPrijs();
		}
		return total;
	}
}
