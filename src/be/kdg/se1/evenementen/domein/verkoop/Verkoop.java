package be.kdg.se1.evenementen.domein.verkoop;

import be.kdg.se1.evenementen.domein.klant.Klant;

import java.time.LocalDate;
import java.util.UUID;

public abstract class Verkoop {

	private  Klant klant;
	private final Wijze wijze;
	private final LocalDate datum;
	private final UUID verkoopnr;
	private Betaling betaling;
	private boolean compleet = false;

	public Verkoop(Klant klant, Wijze wijze) {
		this.klant = klant;
		this.wijze = wijze;
		datum = LocalDate.now();
		verkoopnr = UUID.randomUUID();
	}


	@Override
	public String toString() {
		return "Verkoop{" +
			"klant=" + klant +
			", wijze=" + wijze +
			", datum=" + datum +
			", verkoopnr=" + verkoopnr +
			'}';
	}

	public abstract void add(Object product);

	public boolean isBetaald() {
		return betaling != null;
	}

	public void setCompleet(boolean compleet) {
		this.compleet = compleet;
	}

	public UUID getNummer() {
		return verkoopnr;
	}

	public  Betaling betaal(int bedrag, Betaling.Wijze wijze){
		if (bedrag < getTotal()) return null;
		betaling = new Betaling(bedrag,wijze);
		return betaling;
	}

	protected abstract int getTotal();

	;


	public enum Wijze{
		ONLINE,
		BUREAU
	}
}
