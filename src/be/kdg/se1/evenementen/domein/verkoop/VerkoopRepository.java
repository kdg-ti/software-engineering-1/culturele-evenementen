package be.kdg.se1.evenementen.domein.verkoop;

import be.kdg.se1.evenementen.domein.verkoop.Verkoop;

import java.util.*;

/**
 * Author: derijkej
 */
public class VerkoopRepository {
	private Map<UUID,Verkoop> entities= new HashMap<>();
	;

	/**
	 * updates and adds if not existing
	 * @param verkoop
	 */
	public void update(Verkoop verkoop){
		entities.put(verkoop.getNummer(),verkoop);
	}

	public void insert(TicketVerkoop verkoop) {
		update(verkoop);
	}
}
