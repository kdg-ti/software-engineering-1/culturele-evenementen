package be.kdg.se1.evenementen.domein.voostelling;

import java.time.LocalDate;
import java.util.*;

public class Evenement {
	private String naam;
	private String beschrijving;
	private LocalDate begindatum;
	private LocalDate einddatum;
	private int seizoen;// 2021 = seizoen 2021-2022
	private Map<UUID, Voorstelling> voorstellingen = new HashMap<>();

	public Evenement(String naam, String beschrijving, LocalDate begindatum, LocalDate einddatum, int seizoen) {
		this.naam = naam;
		this.beschrijving = beschrijving;
		this.begindatum = begindatum;
		this.einddatum = einddatum;
		this.seizoen = seizoen;
	}

	public void addVoorstelling(Voorstelling voorstelling) {
		voorstellingen.put(voorstelling.getVoorstellingnr(),voorstelling);
	}

	public String getNaam() {
		return naam;
	}

	public String getBeschrijving() {
		return beschrijving;
	}

	public LocalDate getBegindatum() {
		return begindatum;
	}

	public LocalDate getEinddatum() {
		return einddatum;
	}

	public int getSeizoen() {
		return seizoen;
	}

	public Collection<Voorstelling> getVoorstellingen() {
		return voorstellingen.values();
	}

	public boolean isActive(LocalDate datum) {
		return (getBegindatum().isBefore(datum)|| getBegindatum().isEqual(datum))
		&& (getEinddatum().isAfter(datum)|| getEinddatum().isEqual(datum));
	}
}