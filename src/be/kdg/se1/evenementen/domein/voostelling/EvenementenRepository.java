package be.kdg.se1.evenementen.domein.voostelling;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class EvenementenRepository {
	private List<Evenement> entities = new ArrayList<>();

	public void add(Evenement evenement) {
		entities.add(evenement);
	}

	public List<Evenement> getAll() {
		return entities;
	}

	public List<Voorstelling> getAllVoorstellingen(LocalDate datum) {
		List<Voorstelling> resultaat = new ArrayList<>();
		for(Evenement evenement: getAll()){
			if (evenement.isActive(datum)){
				for (Voorstelling voorstelling:evenement.getVoorstellingen()){
					if(voorstelling.getDatum().isEqual(datum)){
						resultaat.add(voorstelling);
					}
				}
			}
		}
		return resultaat;
	}

}
