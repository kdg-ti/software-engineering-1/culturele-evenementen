package be.kdg.se1.evenementen.domein.voostelling;

import be.kdg.se1.evenementen.domein.infrastructuur.Zaal;
import be.kdg.se1.evenementen.domein.infrastructuur.Zetel;
import be.kdg.se1.evenementen.domein.infrastructuur.ZetelRang;
import be.kdg.se1.evenementen.domein.verkoop.KortingType;
import be.kdg.se1.evenementen.domein.verkoop.ProductType;
import be.kdg.se1.evenementen.domein.verkoop.TicketReservatie;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static be.kdg.se1.evenementen.domein.verkoop.ProductType.TICKET;
import static be.kdg.se1.evenementen.domein.verkoop.TicketReservatie.Toestand.NIEUW;

public class Voorstelling {
	private UUID voorstellingnr;
	private String naam;
	private String beschrijving;
	private LocalDateTime tijdstip;
	private int duur;// in minuten
	private Zaal zaal;
	private List<TicketReservatie> tickets = new ArrayList<>();

	public Voorstelling(String naam, String beschrijving, LocalDateTime tijdstip, int duur, Zaal zaal) {
		this.naam = naam;
		this.beschrijving = beschrijving;
		this.tijdstip = tijdstip;
		this.duur = duur;
		voorstellingnr = UUID.randomUUID();
		this.zaal = zaal;
		initTickets();
	}

	private void initTickets() {
		for (Zetel zetel : zaal.getZetels()) {
			tickets.add(new TicketReservatie(zetel, this));
		}
	}


	public UUID getVoorstellingnr() {
		return voorstellingnr;
	}

	public String getNaam() {
		return naam;
	}

	public String getBeschrijving() {
		return beschrijving;
	}

	public LocalDate getDatum() {
		return tijdstip.toLocalDate();
	}

	public int getDuur() {
		return duur;
	}

	public Zaal getZaal() {
		return zaal;
	}


	@Override
	public String toString() {
		return "Voorstelling{" +
			"naam='" + naam + '\'' +
			", tijdstip=" + tijdstip +
			", zaal=" + zaal +
			'}';
	}

	public List<TicketReservatie> getTickets(ProductType type) {
		return getTickets(type, null);
	}

	public List<TicketReservatie> getTickets(ProductType type, TicketReservatie.Toestand toestand) {
		return getTickets(type, toestand, -1, -1);
	}

	/**
	 * Als een parameter NULL of -1 is wordt er geen rekening mee gehouden
	 * @param type
	 * @param toestand
	 * @param rang
	 * @param max Maximum aantal terug te geven producten
	 * @return
	 */
	public List<TicketReservatie> getTickets(
		ProductType type,
		TicketReservatie.Toestand toestand,
		int rang,
		int max) {
		List<TicketReservatie> resultaat = new ArrayList<>();
		for (TicketReservatie t : tickets) {
			if (max > 0 && resultaat.size() >= max) break;
			if (t.matches(toestand, type, rang)) resultaat.add(t);
		}
		return resultaat;
	}
}
