package be.kdg.se1.evenementen.ui;

import be.kdg.se1.evenementen.controller.TicketReservatieController;
import be.kdg.se1.evenementen.domein.verkoop.Betaling;
import be.kdg.se1.evenementen.domein.verkoop.KortingType;
import be.kdg.se1.evenementen.domein.verkoop.TicketReservatie;
import be.kdg.se1.evenementen.domein.voostelling.Voorstelling;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static be.kdg.se1.evenementen.domein.verkoop.KortingType.*;
import static be.kdg.se1.evenementen.domein.verkoop.ProductType.TICKET;

public class bestelTicketsUI {
	private static String backprompt = "<      ";
	private final TicketReservatieController ctrl;

	public bestelTicketsUI(TicketReservatieController ticketReservatieController) {
		ctrl = ticketReservatieController;
	}


	public void bestel() {
		String email = "tina.trucker@student.kdg.be";
		String paswoord = "secret";
		System.out.println("> OC1: " + email + " meldt aan met paswoord " + paswoord);
		if (! ctrl.aanmelden(email,paswoord)){
			System.out.println("< Aanmelden mislukt voor "+ email);
			return;
		}
		System.out.println(backprompt + "aangemeld: " + ctrl.getKlant());
		System.out.println("> OC2: maakTicketVerkoop");
		ctrl.maakTicketVerkoop();
		System.out.println(backprompt + ctrl.getVerkoop());
		kiesTickets(LocalDate.of(2020,9,25),1,Map.of(BASIS,0, MIN19,1,MIN25,2,PLUS65,3));
		kiesTickets(LocalDate.of(2020,9,30),2,Map.of(BASIS,2, MIN19,0,MIN25,1,PLUS65,2));
		System.out.println("> OC6: einde bestelling");
		int bedrag = ctrl.eindeBestelling();
		System.out.println(backprompt + "Totaalbedrag €" + bedrag);
		Betaling.Wijze betaalwijze = Betaling.Wijze.KREDIETKAART;
		System.out.println("> OC7: betaal met " + betaalwijze + " €" + bedrag);
		System.out.println(backprompt + ctrl.betaal(bedrag,betaalwijze));

	}

	private void kiesTickets(LocalDate datum, 	int rang,Map<KortingType,Integer> bestelLijnen) {
		System.out.println("> OC3: kiesVoorstellingen op " + datum);
		List<Voorstelling> voorstellingen = ctrl.kiesVoorstellingen(datum);
		System.out.println(backprompt + voorstellingen);
		Voorstelling gekozenVoorstelling = voorstellingen.get(0);
		System.out.println("> OC4: Selecteer voorstelling " + gekozenVoorstelling.getNaam());
		List<TicketReservatie> beschikbareTickets = ctrl.selecteerVoorstelling(gekozenVoorstelling);
		System.out.println(backprompt + beschikbareTickets.size() + " tickets beschikbaar");
		System.out.println("> OC5: Systeem kiest individuele tickets met rang " + rang + ": " + bestelLijnen);
		List<TicketReservatie> gereserveerd = ctrl.selecteerTickets(bestelLijnen,rang,TICKET);
		for (TicketReservatie ticket : gereserveerd){
			System.out.println(backprompt + ticket);
		}
	}


}
